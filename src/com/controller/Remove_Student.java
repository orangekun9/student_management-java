package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.dao.Remove_Student_DAO;
import com.dao.View_Student_DAO;
import com.modal.Student;
import com.modal.Validation;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Remove_Student {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Remove_Student window = new Remove_Student();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void loadValues() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		View_Student_DAO s_dao = new View_Student_DAO();
		
		if(model.getRowCount() > 0) {
			for(int i=model.getRowCount() - 1; i > -1; i--) {
				model.removeRow(i);
			}
		}
		
		ArrayList<Student> list = s_dao.readAll();
		for(Student s: list) {
				model.addRow(new Object[] {s.getId(), s.getFname(), s.getLname(), s.getAge(), s.getModule(), s.getBatch(), s.getYear()});
		}
	}
	
	
	/**
	 * Create the application.
	 */
	public Remove_Student() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 833, 583);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("Student Management System");
		label.setFont(new Font("SansSerif", Font.BOLD, 17));
		label.setBounds(291, 11, 261, 33);
		frame.getContentPane().add(label);
		
		JLabel lblRemoveStudentInfo = new JLabel("Remove Student Info");
		lblRemoveStudentInfo.setFont(new Font("SansSerif", Font.BOLD, 17));
		lblRemoveStudentInfo.setBounds(322, 55, 180, 33);
		frame.getContentPane().add(lblRemoveStudentInfo);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(10, 111, 304, 425);
		frame.getContentPane().add(panel);
		
		JLabel label_1 = new JLabel("First Name: ");
		label_1.setBounds(10, 59, 72, 14);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("Last Name: ");
		label_2.setBounds(10, 112, 72, 14);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel("Age: ");
		label_3.setBounds(10, 170, 46, 14);
		panel.add(label_3);
		
		JLabel label_4 = new JLabel("Module: ");
		label_4.setBounds(10, 220, 59, 14);
		panel.add(label_4);
		
		JLabel label_5 = new JLabel("Batch: ");
		label_5.setBounds(10, 268, 46, 14);
		panel.add(label_5);
		
		JLabel label_6 = new JLabel("Year: ");
		label_6.setBounds(10, 319, 46, 14);
		panel.add(label_6);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(102, 56, 126, 20);
		panel.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(102, 109, 126, 20);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(102, 167, 126, 20);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(102, 217, 126, 20);
		panel.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(102, 265, 126, 20);
		panel.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(102, 316, 126, 20);
		panel.add(textField_5);
		
		JLabel errLbl = new JLabel("");
		errLbl.setBounds(32, 405, 235, 14);
		panel.add(errLbl);
		
		Validation val = new Validation();
		JButton button = new JButton("Get Data");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textField_6.getText().equals("") || !val.valid_id(textField_6.getText())) {
					textField.setText("");
					textField_1.setText("");
					textField_2.setText("");
					textField_3.setText("");
					textField_4.setText("");
					textField_5.setText("");
					errLbl.setText("Enter valid Id");
				}
				else {
					errLbl.setText("");
					int id = Integer.parseInt(textField_6.getText());
					View_Student_DAO s_dao =  new View_Student_DAO();
					Student s = s_dao.readOne(id);
					if(s != null) {
						textField.setText(s.getFname());
						textField_1.setText(s.getLname());
						textField_2.setText(String.valueOf(s.getAge()));
						textField_3.setText(s.getModule());
						textField_4.setText(s.getBatch());
						textField_5.setText(String.valueOf(s.getYear()));
					}
					else {
						errLbl.setText("Id is not present");
					}
				}
			}
		});
		button.setFont(new Font("SansSerif", Font.ITALIC, 12));
		button.setBounds(10, 369, 96, 25);
		panel.add(button);
		
		JLabel label_8 = new JLabel("Id: ");
		label_8.setBounds(10, 11, 46, 14);
		panel.add(label_8);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(102, 8, 126, 20);
		panel.add(textField_6);
		
		Remove_Student_DAO s_del = new Remove_Student_DAO();
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals("") || textField_1.getText().equals("") || textField_2.getText().equals("") || textField_3.getText().equals("") || textField_4.getText().equals("") || textField_5.getText().equals("")) {
					errLbl.setText("Incomplete Fields");
				}
				else {
					int id = Integer.parseInt(textField_6.getText());
					if(!val.valid_id(textField_6.getText())){
						textField.setText("");
						textField_1.setText("");
						textField_2.setText("");
						textField_3.setText("");
						textField_4.setText("");
						textField_5.setText("");
						errLbl.setText("Enter valid Id");
					}
					else {
						if(s_del.removeStudent(id)) {
							errLbl.setText("Student info deleted successfully");
							textField.setText("");
							textField_1.setText("");
							textField_2.setText("");
							textField_3.setText("");
							textField_4.setText("");
							textField_5.setText("");
							textField_6.setText("");
							loadValues();
						}
						else {
							errLbl.setText("System Error: Check internal system");
						}
					}
					
				}
			}
		});
		btnDelete.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnDelete.setBounds(116, 369, 89, 25);
		panel.add(btnDelete);
		
		JButton button_2 = new JButton("Clear");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				textField_3.setText("");
				textField_4.setText("");
				textField_5.setText("");
				textField_6.setText("");
				errLbl.setText("");
			}
		});
		button_2.setFont(new Font("SansSerif", Font.ITALIC, 12));
		button_2.setBounds(215, 369, 79, 25);
		panel.add(button_2);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(324, 111, 492, 425);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable(new DefaultTableModel(new Object[][] {}, new String[] { "Id", "FNmae", "LName", "Age","Module","Batch","Year"}));
		scrollPane.setViewportView(table);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new MainPage();
				frame.dispose();
			}
		});
		btnBack.setBounds(727, 0, 89, 23);
		frame.getContentPane().add(btnBack);
		loadValues();
	}
}
