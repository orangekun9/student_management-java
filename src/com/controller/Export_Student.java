package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;

import com.dao.Export_DAO;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;



public class Export_Student {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Export_Student window = new Export_Student();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Export_Student() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 573, 453);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new MainPage();
				frame.dispose();
			}
		});
		btnBack.setBounds(468, 0, 89, 23);
		frame.getContentPane().add(btnBack);
		
		JLabel label = new JLabel("Student Management System");
		label.setFont(new Font("SansSerif", Font.BOLD, 17));
		label.setBounds(152, 11, 261, 33);
		frame.getContentPane().add(label);
		
		JLabel lblExportStudentInfo = new JLabel("Export Student Info");
		lblExportStudentInfo.setFont(new Font("SansSerif", Font.BOLD, 17));
		lblExportStudentInfo.setBounds(184, 55, 180, 33);
		frame.getContentPane().add(lblExportStudentInfo);
		
		Export_DAO exp = new Export_DAO();
		JButton btnSoftwareEngineering = new JButton("Software Engineering");
		btnSoftwareEngineering.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResultSet rs = exp.export("Software Engineering");
				writeToFile(rs, "Software Engineering");
			}
		});
		btnSoftwareEngineering.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnSoftwareEngineering.setBounds(72, 185, 173, 57);
		frame.getContentPane().add(btnSoftwareEngineering);
		
		JButton btnComputerScience = new JButton("Computer Science");
		btnComputerScience.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResultSet rs = exp.export("Computer Science");
				writeToFile(rs, "Computer Science");
			}
		});
		btnComputerScience.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnComputerScience.setBounds(300, 185, 173, 57);
		frame.getContentPane().add(btnComputerScience);
		
		JButton btnInformationTechnology = new JButton("Information Technology");
		btnInformationTechnology.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResultSet rs = exp.export("Information Technology");
				writeToFile(rs, "Information Technology");
			}
		});
		btnInformationTechnology.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnInformationTechnology.setBounds(72, 283, 173, 57);
		frame.getContentPane().add(btnInformationTechnology);
		
		JButton btnSoftwareDevelopment = new JButton("Software Development");
		btnSoftwareDevelopment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResultSet rs = exp.export("Software Development");
				writeToFile(rs, "Software Development");
			}
		});
		btnSoftwareDevelopment.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnSoftwareDevelopment.setBounds(300, 283, 173, 57);
		frame.getContentPane().add(btnSoftwareDevelopment);
	}
	
	private void writeToFile(ResultSet rs, String file){
		try{
			FileWriter outputFile = new FileWriter(file+".csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for(int i=0;i<numColumns;i++){
				printWriter.print(rsmd.getColumnLabel(i+1)+",");
			}
			printWriter.print("\n");
			while(rs.next()){
				for(int i=0;i<numColumns;i++){
					printWriter.print(rs.getString(i+1)+",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		}
		catch(Exception e){e.printStackTrace();}
	}
}
