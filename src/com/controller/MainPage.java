package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainPage {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainPage window = new MainPage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainPage() {		
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 530, 460);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label_1 = new JLabel("Student Management System");
		label_1.setFont(new Font("SansSerif", Font.BOLD, 17));
		label_1.setBounds(135, 21, 261, 33);
		frame.getContentPane().add(label_1);
		
		JButton btnAddStudent = new JButton("Add Student Info");
		btnAddStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Add_Student();
				frame.dispose();
			}
		});
		btnAddStudent.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnAddStudent.setBounds(53, 130, 173, 57);
		frame.getContentPane().add(btnAddStudent);
		
		JButton btnRemoveStudent = new JButton("Remove Student Info");
		btnRemoveStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Remove_Student();
				frame.dispose();
			}
		});
		btnRemoveStudent.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnRemoveStudent.setBounds(284, 130, 173, 57);
		frame.getContentPane().add(btnRemoveStudent);
		
		JButton btnViewStudent = new JButton("View Student Info");
		btnViewStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new View_Student();
				frame.dispose();
			}
		});
		btnViewStudent.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnViewStudent.setBounds(53, 237, 173, 57);
		frame.getContentPane().add(btnViewStudent);
		
		JButton btnUpdateStudentInfo = new JButton("Update Student Info");
		btnUpdateStudentInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Update_Student();
				frame.dispose();
			}
		});
		btnUpdateStudentInfo.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnUpdateStudentInfo.setBounds(284, 237, 173, 57);
		frame.getContentPane().add(btnUpdateStudentInfo);
		
		JButton btnExportStudentInformation = new JButton("Export Student Information");
		btnExportStudentInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Export_Student();
				frame.dispose();
			}
		});
		btnExportStudentInformation.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnExportStudentInformation.setBounds(53, 328, 404, 57);
		frame.getContentPane().add(btnExportStudentInformation);
		
		JButton btnLogout = new JButton("LogOut");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Login();
				frame.dispose();
			}
		});
		btnLogout.setBounds(425, 0, 89, 23);
		frame.getContentPane().add(btnLogout);
	}
}
