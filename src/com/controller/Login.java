package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import com.dao.LoginDAO;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login {

	private JFrame frame;
	private JTextField txtUname;
	private JPasswordField txtPass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 530, 460);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblStudentManagementSystem = new JLabel("Student Management System");
		lblStudentManagementSystem.setFont(new Font("SansSerif", Font.BOLD, 17));
		lblStudentManagementSystem.setBounds(133, 25, 261, 33);
		frame.getContentPane().add(lblStudentManagementSystem);
		
		JLabel lblLogin = new JLabel("Admin Login");
		lblLogin.setFont(new Font("SansSerif", Font.BOLD, 14));
		lblLogin.setBounds(133, 104, 110, 23);
		frame.getContentPane().add(lblLogin);
		
		JLabel lblUsername = new JLabel("Username: ");
		lblUsername.setBounds(133, 173, 70, 14);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password: ");
		lblPassword.setBounds(133, 237, 70, 14);
		frame.getContentPane().add(lblPassword);
		
		txtUname = new JTextField();
		txtUname.setBounds(213, 170, 136, 20);
		frame.getContentPane().add(txtUname);
		txtUname.setColumns(10);
		
		txtPass = new JPasswordField();
		txtPass.setBounds(213, 234, 136, 20);
		frame.getContentPane().add(txtPass);
		
		JLabel errLbl = new JLabel("");
		errLbl.setBounds(153, 377, 261, 14);
		frame.getContentPane().add(errLbl);
		
		LoginDAO login = new LoginDAO();
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtUname.getText().equals("") || txtPass.getText().equals("")) {
					errLbl.setText("Please complete the feilds");
				}
				else {
					if(login.Login(txtUname.getText(), txtPass.getText())) {
						frame.dispose();
						new MainPage();
					}
					else {
						errLbl.setText("Please enter valid details");
					}
				}
			}
		});
		btnLogin.setBounds(201, 319, 89, 23);
		frame.getContentPane().add(btnLogin);
		
		
	}
}
