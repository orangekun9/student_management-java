package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.dao.Update_Student_DAO;
import com.dao.View_Student_DAO;
import com.modal.Student;
import com.modal.Validation;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Update_Student {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTable table;
	private JTextField textField_6;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Update_Student window = new Update_Student();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public void loadValues() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		View_Student_DAO s_dao = new View_Student_DAO();
		
		if(model.getRowCount() > 0) {
			for(int i=model.getRowCount() - 1; i > -1; i--) {
				model.removeRow(i);
			}
		}
		
		ArrayList<Student> list = s_dao.readAll();
		for(Student s: list) {
				model.addRow(new Object[] {s.getId(), s.getFname(), s.getLname(), s.getAge(), s.getModule(), s.getBatch(), s.getYear()});
		}
	}
	

	/**
	 * Create the application.
	 */
	public Update_Student() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 835, 587);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("Student Management System");
		label.setFont(new Font("SansSerif", Font.BOLD, 17));
		label.setBounds(263, 15, 261, 33);
		frame.getContentPane().add(label);
		
		JLabel lblRemoveStudentInfo = new JLabel("Update Student Info");
		lblRemoveStudentInfo.setFont(new Font("SansSerif", Font.BOLD, 17));
		lblRemoveStudentInfo.setBounds(298, 59, 180, 33);
		frame.getContentPane().add(lblRemoveStudentInfo);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(10, 115, 304, 425);
		frame.getContentPane().add(panel);
		
		JLabel label_2 = new JLabel("First Name: ");
		label_2.setBounds(10, 59, 72, 14);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel("Last Name: ");
		label_3.setBounds(10, 112, 72, 14);
		panel.add(label_3);
		
		JLabel label_4 = new JLabel("Age: ");
		label_4.setBounds(10, 170, 46, 14);
		panel.add(label_4);
		
		JLabel label_5 = new JLabel("Module: ");
		label_5.setBounds(10, 220, 59, 14);
		panel.add(label_5);
		
		JLabel label_6 = new JLabel("Batch: ");
		label_6.setBounds(10, 268, 46, 14);
		panel.add(label_6);
		
		JLabel label_7 = new JLabel("Year: ");
		label_7.setBounds(10, 319, 46, 14);
		panel.add(label_7);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(102, 56, 126, 20);
		panel.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(102, 109, 126, 20);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(102, 167, 126, 20);
		panel.add(textField_2);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(102, 265, 126, 20);
		panel.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(102, 316, 126, 20);
		panel.add(textField_5);
		
		JComboBox module1 = new JComboBox();
		module1.setModel(new DefaultComboBoxModel(new String[] {"", "Software Engineering", "Computer Science", "Information Technology", "Software Development"}));
		module1.setBounds(102, 217, 162, 20);
		panel.add(module1);
		
		JLabel errLbl = new JLabel("");
		errLbl.setBounds(32, 405, 235, 14);
		panel.add(errLbl);
		
		Validation val = new Validation();
		JButton btnGetData = new JButton("Get Data");
		btnGetData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textField_6.getText().equals("") || !val.valid_id(textField_6.getText())) {
					textField.setText("");
					textField_1.setText("");
					textField_2.setText("");
					module1.setSelectedItem("");
					textField_4.setText("");
					textField_5.setText("");
					errLbl.setText("Enter valid Id");
				}
				else {
					errLbl.setText("");
					int id = Integer.parseInt(textField_6.getText());
					View_Student_DAO s_dao =  new View_Student_DAO();
					Student s = s_dao.readOne(id);
					if(s != null) {
						textField.setText(s.getFname());
						textField_1.setText(s.getLname());
						textField_2.setText(String.valueOf(s.getAge()));
						module1.setSelectedItem(s.getModule());
						textField_4.setText(s.getBatch());
						textField_5.setText(String.valueOf(s.getYear()));
					}
					else {
						errLbl.setText("Id is not present");
					}
				}
			}
		});
		btnGetData.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnGetData.setBounds(10, 369, 96, 25);
		panel.add(btnGetData);
		
		JLabel lblId = new JLabel("Id: ");
		lblId.setBounds(10, 11, 46, 14);
		panel.add(lblId);
		
		textField_6 = new JTextField();
		textField_6.setBounds(102, 8, 126, 20);
		panel.add(textField_6);
		textField_6.setColumns(10);
		
		JButton btnUpdate = new JButton("Update");
		Update_Student_DAO s_up = new Update_Student_DAO();
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals("") || textField_1.getText().equals("") || textField_2.getText().equals("") || textField_4.getText().equals("") || textField_5.getText().equals("")) {
					errLbl.setText("Incomplete Fields");
				}
				else {
					int id = Integer.parseInt(textField_6.getText());
					int age = Integer.parseInt(textField_2.getText());
					int year = Integer.parseInt(textField_5.getText());
					if(!val.valid_id(textField_6.getText())){
						textField.setText("");
						textField_1.setText("");
						textField_2.setText("");
						module1.setSelectedItem("");
						textField_4.setText("");
						textField_5.setText("");
						errLbl.setText("Enter valid Id");
					}
					else {
						if(s_up.updateStudent(id, textField.getText(), textField_1.getText(), age, module1.getSelectedItem().toString(), textField_4.getText(), year)) {
							errLbl.setText("Student info updated successfully");
							loadValues();
						}
						else {
							errLbl.setText("System Error: Check internal system");
						}
					}
					
				}
			}
		});
		btnUpdate.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnUpdate.setBounds(116, 369, 89, 25);
		panel.add(btnUpdate);
		
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				module1.setSelectedItem("");
				textField_4.setText("");
				textField_5.setText("");
				textField_6.setText("");
				errLbl.setText("");
			}
		});
		btnClear.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnClear.setBounds(215, 369, 79, 25);
		panel.add(btnClear);
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(320, 115, 492, 425);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable(new DefaultTableModel(new Object[][] {}, new String[] { "Id", "FNmae", "LName", "Age","Module","Batch","Year"}));
		scrollPane.setViewportView(table);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new MainPage();
				frame.dispose();
			}
		});
		btnBack.setBounds(738, 0, 81, 23);
		frame.getContentPane().add(btnBack);
		loadValues();
	}
}
