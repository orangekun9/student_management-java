package com.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.dao.View_Student_DAO;
import com.modal.Student;

import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class View_Student {

	private JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View_Student window = new View_Student();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void loadValues() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		View_Student_DAO s_dao = new View_Student_DAO();
		
		if(model.getRowCount() > 0) {
			for(int i=model.getRowCount() - 1; i > -1; i--) {
				model.removeRow(i);
			}
		}
		
		ArrayList<Student> list = s_dao.readAll();
		for(Student s: list) {
				model.addRow(new Object[] {s.getId(), s.getFname(), s.getLname(), s.getAge(), s.getModule(), s.getBatch(), s.getYear()});
		}
	}
	
	/**
	 * Create the application.
	 */
	public View_Student() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 586, 587);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(37, 90, 492, 425);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable(new DefaultTableModel(new Object[][] {}, new String[] { "Id", "FNmae", "LName", "Age","Module","Batch","Year"}));
		scrollPane.setViewportView(table);
		
		JLabel label = new JLabel("Student Management System");
		label.setFont(new Font("SansSerif", Font.BOLD, 17));
		label.setBounds(156, 0, 261, 33);
		frame.getContentPane().add(label);
		
		JLabel lblViewStudentInfo = new JLabel("View Student Info");
		lblViewStudentInfo.setFont(new Font("SansSerif", Font.BOLD, 17));
		lblViewStudentInfo.setBounds(192, 44, 180, 33);
		frame.getContentPane().add(lblViewStudentInfo);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new MainPage();
				frame.dispose();
			}
		});
		btnBack.setBounds(494, 0, 76, 23);
		frame.getContentPane().add(btnBack);
		loadValues();
	}
}
