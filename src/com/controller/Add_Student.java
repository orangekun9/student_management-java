package com.controller;

import java.awt.EventQueue;
import java.util.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.dao.Add_Student_DAO;
import com.dao.View_Student_DAO;
import com.modal.Student;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Add_Student {

	private JFrame frame;
	private JTextField fname;
	private JTextField lname;
	private JTextField age;
	private JTextField batch;
	private JTextField year;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Add_Student window = new Add_Student();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void loadValues() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		View_Student_DAO s_dao = new View_Student_DAO();
		
		if(model.getRowCount() > 0) {
			for(int i=model.getRowCount() - 1; i > -1; i--) {
				model.removeRow(i);
			}
		}
		
		ArrayList<Student> list = s_dao.readAll();
		for(Student s: list) {
				model.addRow(new Object[] {s.getId(), s.getFname(), s.getLname(), s.getAge(), s.getModule(), s.getBatch(), s.getYear()});
		}
	}

	/**
	 * Create the application.
	 */
	public Add_Student() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 854, 586);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("Student Management System");
		label.setFont(new Font("SansSerif", Font.BOLD, 17));
		label.setBounds(289, 11, 261, 33);
		frame.getContentPane().add(label);
		
		JLabel lblAddStudentInfo = new JLabel("Add Student Info");
		lblAddStudentInfo.setFont(new Font("SansSerif", Font.BOLD, 17));
		lblAddStudentInfo.setBounds(336, 55, 180, 33);
		frame.getContentPane().add(lblAddStudentInfo);
		
		JPanel panel = new JPanel();
		panel.setBounds(26, 111, 304, 425);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblFirstName = new JLabel("First Name: ");
		lblFirstName.setBounds(10, 48, 72, 14);
		panel.add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last Name: ");
		lblLastName.setBounds(10, 98, 72, 14);
		panel.add(lblLastName);
		
		JLabel lblAge = new JLabel("Age: ");
		lblAge.setBounds(10, 157, 46, 14);
		panel.add(lblAge);
		
		JLabel lblModule = new JLabel("Module: ");
		lblModule.setBounds(10, 210, 59, 14);
		panel.add(lblModule);
		
		JLabel lblBatch = new JLabel("Batch: ");
		lblBatch.setBounds(10, 268, 46, 14);
		panel.add(lblBatch);
		
		JLabel lblYear = new JLabel("Year: ");
		lblYear.setBounds(10, 319, 46, 14);
		panel.add(lblYear);
		
		fname = new JTextField();
		fname.setBounds(102, 45, 126, 20);
		panel.add(fname);
		fname.setColumns(10);
		
		lname = new JTextField();
		lname.setBounds(102, 95, 126, 20);
		panel.add(lname);
		lname.setColumns(10);
		
		age = new JTextField();
		age.setBounds(102, 154, 126, 20);
		panel.add(age);
		age.setColumns(10);
		
		batch = new JTextField();
		batch.setBounds(102, 265, 126, 20);
		panel.add(batch);
		batch.setColumns(10);
		
		year = new JTextField();
		year.setBounds(102, 316, 126, 20);
		panel.add(year);
		year.setColumns(10);
		
		JComboBox module1 = new JComboBox();
		module1.setModel(new DefaultComboBoxModel(new String[] {"", "Software Engineering", "Computer Science", "Information Technology", "Software Development"}));
		module1.setBounds(102, 207, 162, 20);
		panel.add(module1);
		
		
		JLabel errLbl = new JLabel("");
		errLbl.setBounds(59, 411, 235, 14);
		panel.add(errLbl);
		Add_Student_DAO s_add = new Add_Student_DAO(); 
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(fname.getText().equals("") || lname.getText().equals("") || age.getText().equals("") || batch.getText().equals("") || year.getText().equals("")) {
					errLbl.setText("Please complete the feilds");
				}
				else {
					int age1 = Integer.parseInt(age.getText());
					int year1 = Integer.parseInt(year.getText());
					if(s_add.addStudent(fname.getText(), lname.getText(), age1, module1.getSelectedItem().toString(), batch.getText(), year1)) {
						errLbl.setText("Student information has been added");
						loadValues();
					}
					else {
						errLbl.setText("System Error: Check internal system");
					}
				}
			}
		});
		btnAdd.setFont(new Font("SansSerif", Font.ITALIC, 12));
		btnAdd.setBounds(102, 369, 84, 25);
		panel.add(btnAdd);
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(336, 111, 492, 425);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable(new DefaultTableModel(new Object[][] {}, new String[] { "Id", "FNmae", "LName", "Age","Module","Batch","Year"}));
		scrollPane.setViewportView(table);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new MainPage();
				frame.dispose();
			}
		});
		btnBack.setBounds(770, 0, 68, 23);
		frame.getContentPane().add(btnBack);
		this.loadValues();
		
		
	}
}
