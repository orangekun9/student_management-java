package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import com.modal.Student;
import com.utils.DbConnector;

public class View_Student_DAO {
	
	public ArrayList<Student> readAll(){
		Connection con = DbConnector.getConnection();
		try {
			ArrayList<Student> list = new ArrayList<Student>();
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from student");
			while(rs.next()) {
				Student s = new Student();
				s.setId(rs.getInt("id"));
				s.setFname(rs.getString("fname"));
				s.setLname(rs.getString("lname"));
				s.setAge(rs.getInt("age"));
				s.setModule(rs.getString("module"));
				s.setBatch(rs.getString("batch"));
				s.setYear(rs.getInt("year"));
				list.add(s);
			}
			return list;
			
		}catch (Exception exc) {
			exc.printStackTrace();
		}
		return null;
	}
	
	public Student readOne(int id) {
		Connection connection = DbConnector.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement("select * from Student where id = ?");
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			Student s = new Student();
			while(rs.next()) {
				s.setId(rs.getInt("id"));
				s.setFname(rs.getString("fname"));
				s.setLname(rs.getString("lname"));
				s.setAge(rs.getInt("age"));
				s.setModule(rs.getString("module"));
				s.setBatch(rs.getString("batch"));
				s.setYear(rs.getInt("year"));
			}
			return s;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
