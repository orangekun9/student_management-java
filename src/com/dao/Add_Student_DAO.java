package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.utils.DbConnector;

public class Add_Student_DAO {
	public boolean addStudent(String fname, String lname, int age, String module, String batch, int year) {
		Connection connection = DbConnector.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement("insert into Student(fname, lname, age, module, batch, year)" + "values (?,?,?,?,?,?)");
			statement.setString(1, fname);
			statement.setString(2, lname);
			statement.setInt(3, age);
			statement.setString(4, module);
			statement.setString(5, batch);
			statement.setInt(6, year);
			statement.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
