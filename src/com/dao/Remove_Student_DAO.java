package com.dao;

import java.sql.*;
import java.sql.Connection;
import java.sql.SQLException;

import com.utils.DbConnector;

public class Remove_Student_DAO {
	public boolean removeStudent(int id) {
		Connection connection = DbConnector.getConnection();
		try {
			CallableStatement stmt = connection.prepareCall("{call del_pro(?)}");
			stmt.setInt(1, id);
			stmt.execute();
			stmt.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
