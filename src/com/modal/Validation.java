package com.modal;

import java.util.ArrayList;

import com.dao.View_Student_DAO;

public class Validation {
	View_Student_DAO v = new View_Student_DAO();
	ArrayList<Student> list = v.readAll();
	public boolean valid_id(int id) {
		for(Student s: list) {
			if(s.getId() == id) {
				return true;
			}
		}
		return false;
	}
	
	public boolean valid_id(String id) {
		int idd = Integer.parseInt(id);
		for(Student s: list) {
			if(s.getId() == idd) {
				return true;
			}
		}
		return false;
	}
}
